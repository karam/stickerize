#!/bin/sh -e

[ $# -ne 1 ] && { echo "Usage: $(basename "$0") SOURCE_IMAGE" >&2 ;	exit 1 ; }

RES="$(identify "$1" | cut -c$(($(echo -n "$1" | wc -m)+1))- | cut -d' ' -f3 | \
	cut -d' ' -f2)"
 
conv() {
	[ "$(echo $RES | cut -d'x' -f1)" -ge "$(echo $RES | cut -d'x' -f2)" ] && \
		echo 512x || echo x512
}

convert -geometry "$(conv)" "$1" \
	"$(echo $1 | sed 's/\.[^.]*$//').stickerized.png"
