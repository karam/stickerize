## Stickerize

A small script to automatically convert an image to a Telegram-sticker-ready
one as mandated by [@Stickers](https://t.me/Stickers).

You should have [imagemagick](https://imagemagick.org/) installed.

### Usage

```sh
stickerize SOURCE_IMAGE.jpg # (or .png or anything accepted by imagemagick)
```

Which will produce `SOURCE_IMAGE.sticker.png` ready to be added to your
stickerpack.
